import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  public appPages = [
    {
      title: 'Acceuil',
      url: '/home',
      icon: 'home'
    },
    {
      title: 'Comment ça marche ',
      url: '/works',
      icon: 'list'
    },
    {
      title: 'A Propos',
      url: '/about',
      icon: 'list'
    },
    {
      title: 'Recommander à un ami',
      url: '/list',
      icon: 'list'
    },
    {
      title: 'Soutenir l\'application',
      url: '/soutien',
      icon: 'list'
    },
    {
      title: 'S\'abonner',
      url: '/abonnement',
      icon: 'list'
    },
    {
      title: 'recherche',
      url: '/search',
      icon: 'list'
    },
    {
      title: 'Aide',
      url: '/help',
      icon: 'list'
    }
  ];

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }
}
