import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SoutienPage } from './soutien.page';

describe('SoutienPage', () => {
  let component: SoutienPage;
  let fixture: ComponentFixture<SoutienPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SoutienPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SoutienPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
